import { createAction } from 'redux-act';

export const selectButton = createAction('Select button in search page');
