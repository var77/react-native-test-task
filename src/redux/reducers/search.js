import { createReducer } from 'redux-act';

import actions from '../actions';
import { defaultState } from '../../configs/constants';

export default createReducer({
    [actions.search.selectButton] (state, payload) {
        return { ...state, selectedBtn: payload.btn };
    }
}, defaultState.search)
