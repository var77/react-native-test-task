import { combineReducers } from 'redux';

import searchReducer from './search';
import galleryReducer from './gallery';

export default combineReducers({ search: searchReducer, gallery: galleryReducer });
