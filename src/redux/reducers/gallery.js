import { createReducer } from 'redux-act';

import actions from '../actions';
import { defaultState } from '../../configs/constants';

export default createReducer({

}, defaultState.gallery)
