const buttons = [
    { label: 'Тест 1', key: 'btn1' },
    { label: 'Тест 2', key: 'btn2' },
    { label: 'Тест 3', key: 'btn3' },
    { label: 'Тест 4', key: 'btn4' },
];

export const defaultState = {
    search: {
        buttons,
        selectedBtn: buttons[0]
    },
    gallery: {
        posts: [
            { key: 1, img: `https://i.picsum.photos/id/${Math.floor(Math.random() * 100)}/600/300.jpg`, title: 'Title', date: 'Четверг' },
            { key: 2, img: `https://i.picsum.photos/id/${Math.floor(Math.random() * 100)}/600/300.jpg`, title: 'Title 2', date: 'Пятница' },
            { key: 3, img: `https://i.picsum.photos/id/${Math.floor(Math.random() * 100)}/600/300.jpg`, title: 'Title 3', date: 'Пятница' },
            { key: 4, img: `https://i.picsum.photos/id/${Math.floor(Math.random() * 100)}/600/300.jpg`, title: 'Title 4', date: 'Пятница' },
        ]
    }
};
