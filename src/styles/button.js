export default {
    normal: {
        borderRadius: 20,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    selected: {
        backgroundColor: '#FBE74D'
    },
    md: {
        width: 200,
        height: 40
    },
    sm: {
        width: 100,
        height: 35
    }
}
