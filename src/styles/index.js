export { default as ButtonStyles } from './button';

export default {
    flex: {
        flex: 1
    },
    flexCenter: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    flexRow: {
        flexDirection: 'row'
    },
    searchPageText: {
        alignSelf: 'flex-start',
        color: '#9E9E9E',
        marginTop: 10
    },
    searchButtonsContainer: {
        flex: .4,
        width: '100%'
    },
    searchContainer: {
        paddingHorizontal: 20,
        paddingVertical: 30,
        justifyContent: 'space-between'
    },
    post: {
        width: '100%',
        flex: .3,
        marginBottom: 30,
    },
    postImage: {
        borderRadius: 25,
        width: 320,
        height: 180
    },
    postTitle: {
        fontWeight: 'bold',
        fontSize: 14,
        marginTop: 2
    },
    postDate: {
        fontSize: 11,
        color: '#9E9E9E',
        marginTop: 5
    },
    galleryContainer: {
        paddingTop: 40
    },
    searchPageButton: {
        marginTop: 20
    }
}
