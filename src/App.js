import React from 'react';
import {
  StatusBar,
} from 'react-native';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';

import { Router, Stack, Scene } from 'react-native-router-flux';

import TabIcon from './components/TabIcon';

import Search from './scenes/Search';
import Gallery from './scenes/Gallery';

import reducer from './redux/reducers';

import { defaultState }from './configs/constants'

const store = createStore(reducer, defaultState);

const App = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
        <Provider store={store}>
            <Router>
                <Stack key="root" hideNavBar>
                    <Scene
                        key="tabbar"
                        tabs={true}
                        tabBarStyle={{ backgroundColor: '#FFFFFF' }}
                        showLabel={false}
                    >
                      <Scene key="liked" component={Search} icon={({ selected }) => <TabIcon selected={selected} name='heart-o'/>} />
                      <Scene key="chat" component={Search} icon={({ selected }) => <TabIcon selected={selected} name='comment-o'/>} />
                      <Scene key="search-wrapper" initial showLabel={false} icon={({ selected }) => <TabIcon selected={selected} name='search'/>} >
                          <Scene key="search" title="Просмотр" component={Search} initial />
                          <Scene key="gallery" component={Gallery} hideNavBar  />
                      </Scene>
                      <Scene key="profile" component={Search} icon={({ selected }) => <TabIcon selected={selected} name='user-o'/>} />
                    </Scene>
                </Stack>
            </Router>
        </Provider>
    </>
  );
};

export default App;
