import React from 'react';
import { TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/dist/FontAwesome';

const TabIcon = ({ selected, name }) => {
    return (
        <TouchableOpacity>
            <Icon name={name} size={30} style={{opacity: selected ? 1 : 0.7 }}/>
        </TouchableOpacity>
    );
};

export default TabIcon;
