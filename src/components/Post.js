import React from 'react';
import { TouchableOpacity, Image, Text } from 'react-native';

import styles from '../styles';

const Post = ({ title, date, img, onPress }) => {
    return (
        <TouchableOpacity style={[styles.flex, styles.flexCenter, styles.post]} onPress={onPress}>
            <Image
                style={[styles.postImage]}
                source={{uri: img }}
            />
            <Text style={[styles.postTitle]}>{title}</Text>
            <Text style={[styles.postDate]}>{date}</Text>
        </TouchableOpacity>
    );
};

export default Post;
