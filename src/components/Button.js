import React from 'react';
import { TouchableOpacity, Text } from 'react-native';

import { ButtonStyles } from '../styles';

const Button = ({ selected, label, onPress, size = 'md', style = {} }) => {
    return (
        <TouchableOpacity style={[ButtonStyles.normal, selected ? ButtonStyles.selected : {}, ButtonStyles[size], style ]} onPress={onPress}>
            <Text>{label}</Text>
        </TouchableOpacity>
    );
};

export default Button;
