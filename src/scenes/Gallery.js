import React from 'react';
import {
    SafeAreaView,
    ScrollView,
    View,
    Text,
    Image
} from 'react-native';
import { connect } from 'react-redux';

import Post from '../components/Post';
import styles from '../styles';


const Gallery = ({ dispatch, posts }) => {
    return (
          <ScrollView contentContainerStyle={[styles.galleryContainer]} contentInsetAdjustmentBehavior="automatic">
              <SafeAreaView>
                  { posts.map(post => <Post {...post} />)}
              </SafeAreaView>
          </ScrollView>
    );
};

export default connect(state => state.gallery)(Gallery);
