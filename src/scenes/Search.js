import React from 'react';
import {
    SafeAreaView,
    View,
    Text
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import Button from '../components/Button';
import styles from '../styles';

import actions from '../redux/actions';

const Search = ({ dispatch, buttons, selectedBtn }) => {
    return (
        <SafeAreaView style={[styles.flex, styles.flexCenter, styles.searchContainer]}>
            <View style={[styles.flex, styles.flexCenter, styles.searchButtonsContainer]}>
                { buttons.map(button => <Button { ...button } style={styles.searchPageButton} selected={selectedBtn === button } onPress={() => dispatch(actions.search.selectButton({ btn: button }))}/>)}
                <Text style={styles.searchPageText}>{selectedBtn.label}</Text>
            </View>
            <Button size='sm' label='Далее' onPress={() => Actions.gallery() }/>
        </SafeAreaView>
    );
};

export default connect(state => state.search)(Search);
